require 'date'
require 'yaml'

# Generates the Markdown used by the `/releases/` page based on monthly
# release blog posts in this repository
class ReleaseList
  # Don't generate for versions prior to 8.0
  # CUTOFF = Date.new(2015, 9, 22)
  # Don't generate for versions prior to 9.2
  # 9.2 is the first relase post with YAML features
  CUTOFF = Date.new(2017, 5, 22)

  def count(number = StringIO.new)
    # We started release posts with YAML features in 9.2 on May 22nd, 2017.
    # There were 49 releases prior to that on the 22nd until 5.0 on March 22nd, 2013. See - https://about.gitlab.com/community/mvp/
    # There were 5 releases prior to that on or around the 22nd dating back to 3.0.0 release on October 22nd, 2012. See - https://gitlab.com/gitlab-com/www-gitlab-com/issues/5396#note_224693712
    number.puts(release_posts.count + 49 + 5).to_s
    number.string if number.respond_to?(:string)
  end

  def generate(output = StringIO.new)
    release_posts.each do |post|
      output.puts "## [GitLab #{post.version}](#{post.relative_url})"
      output.puts post.date.to_time.strftime('%A, %B %e, %Y').to_s
      output.puts '{: .date}'
      output.puts

      ultimate = []
      premium = []
      starter = []
      core = []
      has_stages = false

      features_array = ["ultimate", "premium", "starter", "core"]

      for feature in features_array

        feature_capitalized = feature.humanize
        output.puts "### #{feature_capitalized}"

        manage = []
        plan = []
        create = []
        verify = []
        package = []
        secure = []
        release = []
        configure = []
        monitor = []
        defend = []

        post.highlights.each do |highlight|
          if highlight.stage == "manage" && highlight.tier[0] == feature
            manage.push(highlight)
            has_stages = true
          elsif highlight.stage == "plan" && highlight.tier[0] == feature
            plan.push(highlight)
            has_stages = true
          elsif highlight.stage == "create" && highlight.tier[0] == feature
            create.push(highlight)
            has_stages = true
          elsif highlight.stage == "verify" && highlight.tier[0] == feature
            verify.push(highlight)
            has_stages = true
          elsif highlight.stage == "package" && highlight.tier[0] == feature
            package.push(highlight)
            has_stages = true
          elsif highlight.stage == "secure" && highlight.tier[0] == feature
            secure.push(highlight)
            has_stages = true
          elsif highlight.stage == "release" && highlight.tier[0] == feature
            release.push(highlight)
            has_stages = true
          elsif highlight.stage == "configure" && highlight.tier[0] == feature
            configure.push(highlight)
            has_stages = true
          elsif highlight.stage == "monitor" && highlight.tier[0] == feature
            monitor.push(highlight)
            has_stages = true
          elsif highlight.stage == "defend" && highlight.tier[0] == feature
            defend.push(highlight)
            has_stages = true
          end
        end
        # post.highlights.each

        stages = [manage, plan, create, verify, package, secure, release, configure, monitor, defend]
        stage_names = ["manage", "plan", "create", "verify", "package", "secure", "release", "configure", "monitor", "defend"]

        def stage_template(output, feature, manage, plan, create, verify, package, secure, release, configure, monitor, defend, stages, stage_names)
          for stage in stages
            stage_index = stages.find_index(stage)
            stage_name = stage_names[stage_index].humanize
            if stage.length.positive?
              output.puts "<div class='stage-wrapper'>"
              output.puts "<h5>#{stage_name}</h5><ul>"
              stage.each do |highlight|
                output.puts "<li>#{highlight}</li>"
              end
              output.puts "</ul></div>"
              output.puts
            end
          end
        end

        stage_template(output, feature, manage, plan, create, verify, package, secure, release, configure, monitor, defend, stages, stage_names)

        if has_stages != true
          post.highlights.each do |highlight|
            if highlight.tier[0] == feature && highlight.tier[0] == "ultimate"
              ultimate.push(highlight)
            elsif highlight.tier[0] == feature && highlight.tier[0] == "premium"
              premium.push(highlight)
            elsif highlight.tier[0] == feature && highlight.tier[0] == "starter"
              starter.push(highlight)
            elsif highlight.tier[0] == feature && highlight.tier[0] == "core"
              core.push(highlight)
            end
          end
          # post.highlights.each

          #TODO: Put these in an array and iterate over them instead of manually specifying.

          if feature == "ultimate"
            ultimate.each do |highlight|
              output.puts "- #{highlight}"
            end
            output.puts
          end

          if feature == "premium"
            premium.each do |highlight|
              output.puts "- #{highlight}"
            end
            output.puts
          end

          if feature == "started"
            starter.each do |highlight|
              output.puts "- #{highlight}"
            end
            output.puts
          end

          if feature == "core"
            core.each do |highlight|
              output.puts "- #{highlight}"
            end
            output.puts
          end

        end
        # if has_stages

      end
      # features in features_array

    end
    # release_posts.each

    # Return the final string if `output` supports it
    output.string if output.respond_to?(:string)
  end

  # Returns an Array of monthly release posts in descending order
  def release_posts
    root = File.expand_path('../source/posts', __dir__)

    # find's `-regex` option is too dumb to do what we want, so use grep too
    find = %(find "#{root}" -type f -iname "*-released.html.md")
    grep = %(grep #{grep_flags} '\\d{4}-\\d{2}-22-gitlab-\\d{1,2}-\\d{1,2}-released')
    sort = %q(sort -n)

    `#{find} | #{grep} | #{sort}`
      .lines
      .map    { |path| ReleasePost.new(path) }
      .reject { |post| post.date < CUTOFF }
      .reverse
  end

  private

  def grep_flags
    # GNU supports PCRE via `-P`; for others (i.e., BSD), we want `-E`
    if `grep -V`.include?('GNU grep')
      '-P'
    else
      '-E'
    end
  end

  class ReleasePost
    attr_reader :filename, :title, :date, :version, :stage

    def initialize(filename)
      @filename = filename.strip

      extract_attributes
    end

    def relative_url
      format('/%<year>d/%<month>0.2d/%<day>0.2d/%<title>s', year: date.year, month: date.month, day: date.day, title: title)
    end

    # Returns an Array of "highlights"
    #
    # If a data file exists for the release post, we extract the feature list
    # from its YAML.
    #
    def highlights
      return @highlights if @highlights

      @highlights =
        if data_file?
          highlights_from_data_file
        end

      @highlights
    end

    private

    class Highlight
      attr_reader :title, :link, :tier, :stage

      def initialize(title, link = nil, tier = nil, stage = nil)
        @title = title
        @link  = link
        @tier = tier
        @stage = stage
      end

      def link?
        link.to_s
      end

      def to_s
        if link?
          "<a href='#{link}'>#{title}</a>"
        else
          title
        end
      end
    end

    def extract_attributes
      match = filename.match(
        /
          (?<date>\d{4}-\d{2}-\d{2})
          -
          (?<title>
            gitlab-
            (?<major>\d{1,2})-(?<minor>\d{1,2})
            -released
          )
        /xi
      )

      @title   = match['title']
      @date    = Date.parse(match['date'])
      @version = "#{match['major']}.#{match['minor']}"
    end

    def data_file?
      File.exist?(data_file_path)
    end

    def data_file_path
      filename = File.basename(@filename, '.html.md').tr('-', '_')
      File.expand_path("../data/release_posts/#{filename}.yml", __dir__)
    end

    def highlights_from_data_file
      features = YAML.safe_load(File.read(data_file_path)).fetch('features', {})

      features
        .values
        .flatten
        .collect do |f|
          link = f['documentation_link'] || f['performance_url']
          Highlight.new(f['name'], link, f['available_in'], f['stage'])
        end
    end

  end
end