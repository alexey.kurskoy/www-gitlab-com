---
layout: handbook-page-toc
title: "IT Help"
---
# Welcome to the IT Help Handbook

The IT Help team is part of the [GitLab Business Ops](/handbook/business-ops) function that guides systems, workflows and processes and is a singular reference point for operational management.

## GET IN TOUCH

* [IT Help Issues](https://gitlab.com/groups/gitlab-com/business-ops/bizops-IT-help/-/issues)
* #it-help on slack and the #it_help channel

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Mission Statement

IT Help will triage all IT related questions as they arise. Build a knowledge base of IT practices and pragmatic problem solving in the handbook. Account management for password resets and lockout. On call support for immediate software and hardware issues during local business hours. Diagnose computer errors and provide technical support. Troubleshoot software and hardware. Support Weekly IT Onboarding Sessions for new Team Members. Train end-users how to setup and use new technologies. Provide technical support over the phone or Web. Use specialized help desk support software to take control of end-users' computers to troubleshoot, diagnose and resolve complex issues.


## Requests for support (2fa, password resets, etc)

Request for support should have an issue open at [IT Help Issues](https://gitlab.com/groups/gitlab-com/business-ops/bizops-IT-help/-/issues). Certain support items like 2FA resets are time sensitive but there are some things you can do to help yourself. For 2FA related problems for your Gitlab accounts, please use your backup codes or try generating [new ones](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#generate-new-recovery-codes-using-ssh).

Follow these steps to successfully set up 2fa for your [Google account](https://support.google.com/accounts/answer/185839?co=GENIE.Platform%3DDesktop&hl=en).

When you get a new device, before you get rid of the old one, make sure to set up your authenticator app on the new device. You will need to log into your accounts with the old device and disable 2fa for your accounts. Then delete the accounts off the old device and re-enable the 2fa on your accounts on the authenticator app on the new device. If you still need a 2fa reset, please reach out to #it_help on slack with the system that needs it and the issue you are encountering. 


We understand sometimes life happens and passwords get forgotten or deleted. If it is for a system that requires immediate access, please reach out on the slack #it_help channel. Provide as much information as possible. Password resets to sensitive systems such as your G Suite account and Okta require a zoom call to verify. 

As a distributed team, our current standard coverage window is ~13:00-22:00 UTC. High volumes of issues being triaged can dictate the delay in response within that window. If the issue is extremely time sensitive and warrants escalation, use judgement on whether or not it can wait until ‘business hours’. Escalated issues should be made through the #it_help slack channel. All other request should have an issue created. 


### How to contact us, or escalate priority issues outside of standard hours:


Slack is not ideal for managing priorities of incoming issues, so we ask that all such requests get created at [IT Help Issues](https://gitlab.com/groups/gitlab-com/business-ops/bizops-IT-help/-/issues) and use [THIS](https://gitlab.com/gitlab-com/business-ops/bizops-IT-help/hd-issue-tracker/issues/new?issuable_template=General%20HelpDesk%20Request) template for general issues. We will triage and address them as soon as we can.  All issues created in the queue are public by default.

Privileged or private communications should be sent to it-help@. 

Screenshots and videos are very helpful when experiencing an issue, especially if there is an error message.


### Instructions on how to create Role-Based Entitlement Templates.

Below is a summary of how to create new templates for a Role-Based Entitlement that does not have one. 

1. Pick a role that is somewhat similar to or look through the templates that already exist. [Issue Template](https://gitlab.com/gitlab-com/access-requests/tree/master/.gitlab/issue_templates)

2. Then copy the markdown and create a new file in the directory. [Acess Request](https://gitlab.com/gitlab-com/access-requests/tree/master/.gitlab/issue_templates)

3. Name it with _ instead of any spaces with the dept at the beginning like Eng_Backend_Engineer_Entitlements_Access_Request.

4. Update the copy to reflect their job title, systems, edit the bottom copy of tech stack provisioners and labels. (which you can assist with)

5. Once it’s prepared, it will need to be reviewed by a manager and director in their department.

6. Then if the management comments their approval, it can be merged.

7. Add to README and AR handbook page.

### Self-Help and Troubleshooting Tips.

This section should provide some quick and easy troubleshooting tips anyone can do to possibly remedy an IT issue before reaching out. This section will be expanding over time so keep an eye out or feel free to contribute if you think something belongs. 

#### Built in Macbook troubleshooting commands.

Macbooks are wonderful laptops but no laptop is without faults, time an again you may come across a "wonky" situation for your Mac but below are some pointers that might fix these situations as they arise. 

*  Reset your [NVRAM and PRAM](https://support.apple.com/en-us/HT204063) - non-volatile random access memory and parameter RAM stores small amount of information on your Mac, if you experience issues related to what's in the Apple article reseting this might help out. 

*  Reset the [SMC](https://support.apple.com/en-us/HT201295) - System Management Controller handles some low-level functions like battery management and if you experience issues with fans or internal ports this could help resolve those issues. (note different models have different reset methods)

*  Apple Diagnostics [Hardware Diagnostics](https://support.apple.com/en-us/HT202731)



#### Password reset for you Macbook?

Did you go out for a long well deserved vacation and come back to a completely blank memory on what your laptop password was? It happens to the best of us! Apple has you covered.

* 1. Power down the MAC.
* 2. Restart the MAC while also concurrently holding down the Command + R keys (to go to recovery mode); release the keys when Apple logo appears.
* 3. Once MAC is restarted select from the menu Utilities > Terminal
* 4. At the window that opens, type resetpassword
* 5. Follow onscreen instructions to reset


#### Repair a disc or Mac storage drive.

Apple's Disk Utility tool can fix a couple specific problems, couple examples are apples are quiting or crashing, files are corrupted, external drives not working, etc. (you can format drives with DU as well)

This is a quick walkthrough on how to check your Macbooks disk and run First Aid

*  If you don't want to reboot the laptop, open Spotlight and search "disk utility".
*  In the Disk Utility App, select your disk (should be named Macintosh HD but you can change that too)
*  At the top you will see "First Aid", click that an hit run. 
*  Your macbook will run First Aid and report any issues plus correct them if it can. 


#### Is your Okta password acting up in Chrome?

If you are having issues with your Okta password in Chrome, the below steps might help out! Keep in mind this will reset chrome to default settings but its easy enough to restore and link data back.

* On your computer, open Chrome.
* At the top right, click More More and then Settings.
* At the bottom, click Advanced.
* Linux, and Mac: Under "Reset Settings," click Restore settings to their original defaults and then Reset Settings.

